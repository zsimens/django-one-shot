from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm


# Create your views here.
def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "list_object": lists,
        }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    items = get_object_or_404(TodoList, id=id)
    context = {
        "items": items,
        }
    return render(request, "todos/details.html", context)

def todo_list_create(request, id):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)
# from django.shortcuts import render, get_object_or_404, redirect
# from recipes.models import Recipe
# from recipes.forms import RecipeForm
# from django.auth.contrib.decorators import login_required

# # Create your views here.
#

# def recipe_list (request):
#     recipes = Recipe.objects.all()
#     context = {
#         "recipe_list": recipes,
#         # "created_on": date,
#     }
#     return render(request, "recipes/list.html", context)

# @login_required
# def create_recipe(request):
#     if request.method == "POST":
#         # use form to validate values and save them to database
#         form = RecipeForm(request.POST)
#         if form.is_valid():
#             recipe = form.save(False)
#             recipe.author = request.user
#             recipe.save()
#             # redirect browser to other page and leave function
#             return redirect("recipe_list")
#         else:
#             # Create an instance of the Django model form class
#             form = RecipeForm()

#         # Put the form in the context
#         context = {
#             "recipe_form": form,
#         }
#         # Render the HTML template with the form
#         return render(request, "recipes/create.html", context)

# def edit_recipe(request, id):
#     recipe = get_object_or_404(Recipe, id=id)
#     if request.method == "POST":
#         form = RecipeForm(request.POST, instance=recipe)
#         if form.is_valid():
#             form.save()
#             return redirect("show_recipes", id=id)
#     else:
#         form = RecipeForm(instance=recipe)

#     context = {
#         "recipe_object": recipe,
#         "recipe_form": form,
#     }
#     return render(request, "posts/edit.html", context)

# @login_required
# def my_recipe_list(request):
#     recipes = Recipe.objects.filter(author=request.user)
#     context = {
#         "recipe_list": recipes,
#     }
#     return render(request, "recipes/list.html", context)
